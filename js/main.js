// Spinner
let loading = document.querySelector("#spinner");
function spinner() {
  setTimeout(function () {
    loading.classList.remove("show");
  }, 2);
}
spinner();

// Sticky Navbar
let navbar = document.querySelector(".nav-bar");

document.addEventListener("scroll", () => {
  let scrollTop = window.scrollY;

  if (scrollTop > 45) {
    navbar.classList.add("sticky-top");
  } else {
    navbar.classList.remove("sticky-top");
  }
});

// JSON: Javascript Object Notation
// fetch
/*
    1- Stampare in console tutto l'array di oggetti
    2- Estrapolare le Categorie UNICHE 
    3- Creare i box delle categorie
    4- Creare le option della select nella barra di ricerca mediante le categorie uniche
    5- Creare le card degli annunci
    6- Implementato la ricerca annunci per box category
    7- Implementare il filtro per Categoria
    8- Implementare il filtro per Prezzo
    9- Implemenatre il filtro per Parola
*/
fetch("./menu.json")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);

    // Array contenente le categorie uniche
    // let categories = [];

    // Catturiamo il contenitore dei box categorie
    // let rowCategories = document.querySelector("#rowCategories");

    // Catturiamo il contenitore del filtro delle categorie
    // let selectCategory = document.querySelector("#container-category");

    // Catturiamo il contenitore dei menu
    let rowMenu = document.querySelector(".rowMenu");

    // Catturiamo label e input per il prezzo
    // let labelPrice = document.querySelector("#labelPrice");
    // let inputPrice = document.querySelector("#inputPrice");

    // Catturiamo il campo per ricercare la parola
    // let inputWord = document.querySelector("#inputWord");

    // data.forEach((element) => {
    //   if (!categories.includes(element.category)) {
    //     categories.push(element.category);
    //   }
    // });

    // categories.forEach((element, index) => {
    //   // 3- Creando i box delle categorie
    //   let div = document.createElement("div");
    //   div.classList.add("col-lg-3", "col-sm-6", "wow");
    //   div.setAttribute("data-category", element);
    //   div.innerHTML = `
    //     <a class="cat-item d-block bg-light text-center rounded p-3" href="#section-annunci">
    //         <div class="icon mb-3">
    //             <img class="img-fluid icon" src="https://picsum.photos/300${+index}" alt="Icon">
    //         </div>
    //         <h6>${element}</h6>
    //     </a>
    //     `;
    //   rowCategories.appendChild(div);

    //   // 4- Creare le option del filtro delle categorie
    //   let option = document.createElement("option");
    //   option.setAttribute("value", element);
    //   option.innerHTML = element;
    //   selectCategory.appendChild(option);
    // });

    // // Animazione box category
    let categoriesBox = document.querySelectorAll(".btnCategory");

    // function handleIntersectionCategory(entries) {
    //   entries.map((entry, index) => {
    //     if (entry.isIntersecting) {
    //       entry.target.classList.add("fadeInUp");
    //       entry.target.classList.remove("opacity-0");
    //       entry.target.style.animationDelay = `${index++}s`;
    //     }
    //   });
    // }

    // let obseverCategory = new IntersectionObserver(handleIntersectionCategory);

    // categoriesBox.forEach((element) => {
    //   obseverCategory.observe(element);
    // });

    // 5- Creare le card degli annunci
    function createMenu(array) {
      // Ripulire il contenitore delle card
      rowMenu.innerHTML = "";

      let i = 1;
      array.forEach((element) => {
        let div = document.createElement("div");
        div.classList.add("col-lg-6");
        div.innerHTML = `
        <div class="d-flex align-items-center">
        <img
          class="flex-shrink-0 img-fluid rounded"
          src="img/menu-${i}.jpg"
          alt=""
          style="width: 80px"
        />
        <div class="w-100 d-flex flex-column text-start ps-4">
          <h5
            class="d-flex justify-content-between border-bottom pb-2"
          >
            <span>${element.menu}</span>
            <span class="text-primary">$${(
              Math.round(element.price * 2) / 2
            ).toFixed(2)}</span>
          </h5>
          <small class="fst-italic"
            >Ipsum ipsum clita erat amet dolor justo diam</small
          >
        </div>
      </div>
            `;
        if (i == 8) {
          i = 1;
        } else {
          i++;
        }
        rowMenu.appendChild(div);
      });
    }

    createMenu(data);

    // 6- Implementato la ricerca annunci per box category
    // Associamo l'evento click ad ogni box category
    categoriesBox.forEach((box) => {
      box.addEventListener("click", () => {
        let filtered = data.filter(
          (element) => element.category === box.getAttribute("data-category")
        );
        createMenu(filtered);
      });
    });

    // 7- Implementare il filtro per Categoria
    function filterByCategory() {
      //   selectCategory.addEventListener("click", () => {
      //     // console.log(selectCategory.value);
      //     if (selectCategory.value === "All") {
      //       createMenu(data);
      //     } else {
      //       let filtered = data.filter(
      //         (element) => element.category === selectCategory.value
      //       );
      //       createMenu(filtered);
      //     }
      //   });
      //   btnBreakfast.addEventListener("click", () => {
      //     // console.log(selectCategory.value);
      //     let filtered = data.filter(
      //       (element) => element.category === selectCategory.value
      //     );
      //     createMenu(filtered);
      //   });
      //   btn.addEventListener("click", () => {
      //     // console.log(selectCategory.value);
      //     let filtered = data.filter(
      //       (element) => element.category === selectCategory.value
      //     );
      //     createMenu(filtered);
      //   });
      //   btn.addEventListener("click", () => {
      //     // console.log(selectCategory.value);
      //     let filtered = data.filter(
      //       (element) => element.category === selectCategory.value
      //     );
      //     createMenu(filtered);
      //   });
    }

    // // 8- Implementare il filtro per Prezzo
    // function filterByPrice() {
    //   let arrayPrice = data.map((element) => Number(element.price));
    //   let maxPrice = Math.max(...arrayPrice);

    //   inputPrice.max = maxPrice;

    //   inputPrice.addEventListener("input", () => {
    //     // console.log(inputPrice.value);
    //     labelPrice.innerHTML = `Prezzo: ${inputPrice.value} &euro;`;

    //     let filtered = data.filter(
    //       (element) => Number(element.price) <= Number(inputPrice.value)
    //     );
    //     createCards(filtered);
    //   });
    // }

    // // 9- Implemenatre il filtro per Parola
    // function filterByWord() {
    //   inputWord.addEventListener("input", () => {
    //     // console.log(inputWord.value);
    //     let filtered = data.filter(
    //       (element) =>
    //         element.name
    //           .toLowerCase()
    //           .includes(inputWord.value.toLowerCase()) ||
    //         element.category
    //           .toLowerCase()
    //           .includes(inputWord.value.toLowerCase())
    //     );

    //     createCards(filtered);
    //   });
    // }

    filterByCategory();
    // filterByPrice();
    // filterByWord();
  });

// COVERFLOW
let swiperCoverflow = new Swiper(".mySwiperCoverflow", {
  effect: "coverflow",
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: "auto",
  coverflowEffect: {
    rotate: 50,
    stretch: 0,
    depth: 100,
    modifier: 1,
    // importante
    slideShadows: false,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  autoplay: {
    delay: 2000,
  },
  loop: true,
});

let swiperCube = new Swiper(".mySwiperCube", {
  effect: "cube",
  grabCursor: true,
  cubeEffect: {
    shadow: true,
    slideShadows: true,
    shadowOffset: 20,
    shadowScale: 0.94,
  },
  autoplay: {
    delay: 2000,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  loop: true,
});

let backToTop = document.querySelector("#backToTop");
document.addEventListener("scroll", () => {
  let scrollTop = window.scrollY;
  console.log(scrollTop);
  if (scrollTop < 500) {
    backToTop.classList.add("d-none");
  } else {
    backToTop.classList.remove("d-none");
  }
  // window.scrollY;
});

let swiperCards = new Swiper(".mySwiperCards", {
  effect: "cards",
  grabCursor: true,
  loop: true,
});

//darkmode implementation with local storage data saves
// Catturo il bottone
let darkmodeSwitch = document.querySelector("#darkmodeSwitch");
// let btnSwitch = document.querySelector(".btnSwitch");

let isClicked = true;

darkmodeSwitch.addEventListener("click", () => {
  console.log("sono qui");
  if (isClicked) {
    document.documentElement.style.setProperty("--dark", "#f1f8ff");
    document.documentElement.style.setProperty("--light", "#0f172b");
    darkmodeSwitch.innerHTML = `<i class="bi bi-toggle-on"></i>`;
    isClicked = false;
    // imposto local storage
    localStorage.setItem("mode", "dark");
  } else {
    document.documentElement.style.setProperty("--light", "#f1f8ff");
    document.documentElement.style.setProperty("--dark", "#0f172b");
    darkmodeSwitch.innerHTML = `<i class="bi bi-toggle-off"></i>`;
    isClicked = true;
    // imposto local storage
    localStorage.setItem("mode", "light");
  }
});

// Leggo valore di una chiave all'interno del local storage
let storageMode = localStorage.getItem("mode");
if (storageMode === "dark") {
  document.documentElement.style.setProperty("--dark", "#f1f8ff");
  document.documentElement.style.setProperty("--light", "#0f172b");
  darkmodeSwitch.innerHTML = `<i class="bi bi-toggle-on"></i>`;
  isClicked = false;
} else {
  document.documentElement.style.setProperty("--light", "#f1f8ff");
  document.documentElement.style.setProperty("--dark", "#0f172b");
  darkmodeSwitch.innerHTML = `<i class="bi bi-toggle-off"></i>`;
  isClicked = true;
}
